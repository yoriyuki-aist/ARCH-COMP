Requirements
------------

-   Java 8 (`java` in `$PATH`)
-   MATLAB R2018b (`matlab` in `$PATH`)
    -   Simulink/Stateflow
    -   Control Systems (or similar, for `ss` function, benchmark `F16`)
    -   Deep Learning (benchmark `SC`)

Installation
------------

Discover the path to the MATLAB installation

    ./falstar-config        # runs MATLAB

Verify that it worked

    cat ~/.falstar/matlab   # check detected path

Output:

    MATLABROOT=/path/to/MATLAB/R2018b

Check whether FalStar can be started

    ./falstar

Output:

	usage: falstar [-agv] [+] file_1 ... file_n
	  -a    ask for additional input files:
			  enter one filename per line followed by a blank line
			  a blank line acknowledges, EOF (CTRL+d) aborts
	  -d    dummy run, parse and validate configuration only
	  -g    show a graphical diagram for each trial
	  -v    be verbose
	   +    no header in csv for next file (data should match previous header)
	bye

Running all the benchmarks
--------------------------

	./runall.sh 			# takes long

Then open `results1/summary.csv` and `results2/summary.csv` in Excel/OpenOffice/Google Sheets.
